import scala.annotation.tailrec

/**
  * Created by Pradeep on 7/14/2017.
  */
object Main {
  def reverseListTailRec(list:List[Int]): List[Int] ={
    def reverseR(result:List[Int], tail:List[Int]):List[Int] = tail match {
      case Nil=>result
      case x::remaining => reverseR(List(x):::result, remaining)
    }
    reverseR(Nil, list)
  }

  def flattenList(list:List[List[Int]]):List[Int] = list match{
    case Nil=>Nil
    case headList::Nil=>headList
    case headList::tailList => headList:::flattenList(tailList)
  }

  def flattenListTailRecursive(list:List[List[Int]]):List[Int] = {
    def flattenInnerItems(results:List[Int], rest:List[List[Int]]):List[Int] = rest match{
      case Nil=>results
      case head::tail=> flattenInnerItems(results:::head, tail)
    }
    flattenInnerItems(list.head, list.tail)
  }

  def deDupeList(list:List[Char]): List[Char] = list match{
    case Nil=>Nil
    case head::Nil=>List(head)
    case head::tail =>{
      if(head==tail.head)deDupeList(tail)else head::deDupeList(tail)
    }
  }

  def packDuplicates(list:List[Char]):List[List[Char]] = list match{
    case Nil=>Nil
    case head::Nil=>List(List(head))
    case head::tail=>{
      if(head==tail.head){
        List(list.takeWhile(char=>char==head)):::packDuplicates(list.dropWhile(item=>item==head))
      }
      else List(head)::packDuplicates(tail)
    }
  }
  def main(args:Array[String]):Unit ={
//    val list = List(1, 1, 2, 3, 5, 8)
//    val reversed = reverseListTailRec(list)
//    println(reversed)
      val flattenedList:List[Int] = flattenList(List(List(3,2), List(8,7), List(5,6)))
    val flattenedListTailRecursive = flattenListTailRecursive(List(List(3,2), List(8,7), List(5,6), List(2,8)))
    println(flattenedList)
    println(flattenedListTailRecursive)

    val chars = List('a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e')

    val deDuped = deDupeList(chars)
    val packed = packDuplicates(chars)
    println(deDuped)
    println(packed)
    val lengthEncoded = packed.map(x=>(x.head, x.length))
    println(lengthEncoded)
  }
}
