val list = List(1, 1, 2, 3, 5, 8)
def findLast(list:List[Int]):Int = list match {
  case Nil=>throw  new NoSuchElementException
  case x::Nil => x
  case x::rest => findLast(rest)
}
val last = findLast(list)

def getPenultimate(list:List[Int]):Int = list match{
  case Nil => throw new NoSuchElementException
  case x::Nil=>throw  new NoSuchElementException
  case x::_::Nil=>x
  case _::tail => getPenultimate(tail)
}

val penultimate = getPenultimate(list)

def getNthElement(n:Int, list:List[Int]):Int = {
 def getNthItem(acc:Int, currentItem: Int, tail:List[Int]):Int = tail match{
   case Nil => if(n-acc==0) currentItem else throw new NoSuchElementException
   case x::_ => if(n-acc==0) currentItem else getNthItem(acc+1, tail.head, tail.tail)
 }
  if(n<0) throw new NoSuchElementException
  else if(n>list.length) throw new NoSuchElementException
  else getNthItem(1, list.head, list.tail)
}

var thirdItem = getNthElement(3, list)

def listCount(list:List[Int]): Int = list match {

  case Nil => -1
  case x::Nil=>1
  case x::tail =>{
    1+ listCount(tail)
  }
}

var count = listCount(2::4::Nil)

def reverseList(list:List[Int]):List[Int] = list match{
  case Nil=> Nil
  case x::tail => reverseList(tail):::List(x)
}
def reverseListTailRec(list:List[Int]): List[Int] ={
  def reverseR(result:List[Int], tail:List[Int]):List[Int] = tail match {
    case Nil=>result
    case x::remaining => reverseR(List(x):::result, remaining)
  }
  reverseR(Nil, list)
}

var reversed = reverseList(list)
//var reversedTailRecursive = reversedTailRecursive(list)
var reversedUsingFold = list.foldLeft(List[Int]())((top, bottom)=>bottom::top)
var counter:Int=0
val listCountUsingFold = list.fold(counter)((_,_)=>{counter+=1; counter})

val palindromeList = List(1,2,3,4,3,2,1)

val firstHalf = list.take(list.length/2)
val secondHalf = list.takeRight(list.length/2)

val firstHalfPalindromeList = palindromeList.take(palindromeList.length/2)
val secondHalfPalindromeList = palindromeList.takeRight(palindromeList.length/2)

val isPalindrome = firstHalf==reverseList(secondHalf)

def flattenList(list:List[List[Int]]):List[Int] = list match{
  case Nil=>Nil
  case headList::Nil=>headList
  case headList::tailList => headList:::flattenList(tailList)
}

def flattenListTailRecursive(list:List[List[Int]]):List[Int] = {
  def flattenInnerItems(results:List[Int], rest:List[List[Int]]):List[Int] = rest match{
    case Nil=>results
    case head::tail=> flattenInnerItems(results:::head, tail)
  }
  flattenInnerItems(list.head, list.tail)
}

val flattenedList = flattenList(List(List(3,2), List(8,7), List(5,6)))
