def deDupeList(list:List[Char]): List[Char] = list match{
  case Nil=>Nil
  case head::Nil=>List(head)
  case head::tail =>{
    if(head==tail.head)deDupeList(tail)else head::deDupeList(tail)
  }
}
val chars = List('a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e')
val deDuped = deDupeList(chars)

def packDuplicates(list:List[Char]):List[List[Char]] = list match{
  case Nil=>Nil
  case head::Nil=>List(List(head))
  case head::tail=>{
    if(head==tail.head){
      List(list.takeWhile(char=>char==head)):::packDuplicates(list.dropWhile(item=>item==head))
    }
    else List(head)::packDuplicates(tail)
  }
}

val packed = packDuplicates(chars)

val lengthEncoded = packed.map(x=>(x.head, x.length))

def duplicateEveryItem(list:List[Char]):List[Char] = list match{
  case Nil=>Nil
  case item::Nil=>item::List(item)
  case item::tail => item::item::duplicateEveryItem(tail)
}

def duplicateEveryItemFlatMap(list:List[Char]) = list.flatMap(char=>List(char, char))

val duplicated = duplicateEveryItem(List('a', 'b', 'c', 'c', 'd'))
val duplicatedUsingFlatMap = duplicateEveryItemFlatMap(List('a', 'b', 'c', 'c', 'd'))

def repeatNumberOfTimes(n:Int, list:List[Char]):List[Char] = {
  def repeatItem(acc:List[Char], counter:Int, number:Int, char:Char):List[Char] = {
    if(counter<number) repeatItem(char::acc,counter+1, number, char)
    else acc
  }
  if(n<0) throw new IllegalArgumentException
  else if(n==0) list
  else{
    list match{
      case Nil=>Nil
      case char::tail=>repeatItem(List[Char](), 0, n, char):::repeatNumberOfTimes(n, tail)
    }
  }
}

def repeatNumberOfTimesFlatMap(n:Int, list:List[Char]) = list flatMap(char=>List.fill(n)(char))

val duplicatedNumberIfTimes = repeatNumberOfTimes(3, List('a', 'b', 'c', 'c', 'd'))
val repeatNumberOfTimesUsingFlatMap = repeatNumberOfTimesFlatMap(3, List('a', 'b', 'c', 'c', 'd'))

def dropEveryNthItem(n:Int, list:List[Char]):List[Char] = {
  var counter = 0
  list.map(char=>{counter+=1; (char, counter)}).filter(item=>item._2%n==0).map(item=>item._1)
}

dropEveryNthItem(4, repeatNumberOfTimesFlatMap(3, List('a', 'b', 'c', 'c', 'd')))

repeatNumberOfTimesFlatMap(3, List('a', 'b', 'c', 'c', 'd')).zipWithIndex.filter(value=>value._2%4==0).map(_._1)

def splitList(divider:Int, list:List[Char])=(divider, list) match {
  case (0, _)=>(list, Nil)

  case (value, listItems) =>{
    if(value>listItems.length) throw new IllegalArgumentException
    else (listItems.take(value), list.drop(divider) )
  }
}

def splitFunctional[A](n: Int, ls: List[A]): (List[A], List[A]) =
  (ls.take(n), ls.drop(n))

splitList(2, List('a', 'b', 'c', 'c', 'd'))
splitFunctional(8, List('a', 'b', 'c', 'c', 'd'))

def sliceElements[A](start:Int, end:Int, list:List[A]) = list.slice(start, end)
sliceElements(1, 4, List('a', 'b', 'c', 'c', 'd'))